class Task {
    constructor(id, task, isActive) {
        this.id = id;
        this.task = task;
        this.isActive = isActive;
    }
}
export default Task;