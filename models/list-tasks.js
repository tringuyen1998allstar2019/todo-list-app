class ListTasks {
    constructor() {
        this.arr = [];
    }
    addTask(task) {
        this.arr.push(task);
    }
    _findIndexTask(id) {
        console.log(id);
        return this.arr.findIndex((t) => t.id == id);
    }
    removeTask(id) {
        let index = this._findIndexTask(id);
        console.log(index);
        if (index !== -1) {
            this.arr.splice(index, 1);
        }
    }
    completedTask(id) {
        let index = this._findIndexTask(id);
        if (index !== -1) {
            this.arr[index].isActive = 0;
        }
    }

}
export default ListTasks;