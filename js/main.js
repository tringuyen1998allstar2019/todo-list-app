import ListTask from "./../models/list-tasks.js";
import Task from "../models/task.js"

/*Lấy ngày hiện tại*/
const getDate = () => {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }
    today = dd + '-' + mm + '-' + yyyy;
    document.getElementById("timeTitle").innerHTML = "Today: " + today;
};
getDate();
//Tạo newlist 
let listTask = new ListTask();
/* RenderTask uncomplete*/
const renderTask = (listTask) => {
    let contentHTMLUncomplete = "";
    let contentHTMLComplete = "";
    listTask.arr.forEach((item) => {
        if (item.isActive === 1) {
            contentHTMLUncomplete += `
        <li class="list-group-item d-flex justify-content-between align-items-center">
        ${item.task}
        <span class="badge badge-primary badge-pill"><i class="fa fa-trash-alt" style="margin-right:5px;color:#dc3545" onclick="deleteTask('${item.id}')"></i><i class="fa fa-check-circle" style="margin-right:5px; color:#6c757d" onclick="completeTask('${item.id}')"></i></span>
      </li>     
        `
        } else if (item.isActive === 0) {
            contentHTMLComplete += `
            <li class="list-group-item d-flex justify-content-between align-items-center">
            ${item.task}
            <span class="badge badge-primary badge-pill"><i class="fa fa-trash-alt" style="margin-right:5px;color:#dc3545" onclick="deleteTask('${item.id}')"></i><i class="fa fa-check-circle" style="margin-right:5px;color:#276738"></i></span>
          </li>     
            `
        }
    })
    document.getElementById("todo").innerHTML = contentHTMLUncomplete;
    document.getElementById("completed").innerHTML = contentHTMLComplete;
};
/**Save to local */
const saveToLocalStorage = () => {
    localStorage.setItem('LIST_TASK', JSON.stringify(listTask.arr));
};
/**Get from local storage */
const getFromLocalStorage = () => {
    if (localStorage.getItem('LIST_TASK')) {
        listTask.arr = JSON.parse(localStorage.getItem('LIST_TASK'));
        renderTask(listTask);
    }
};
getFromLocalStorage();

/*Validate đầu vào*/
const checkInputRequired = (value) => {
    if (!value) {
        document.getElementById("alertInput").style.display = "block";
        return false;
    } else {
        document.getElementById("alertInput").style.display = "none";
        return true;
    }
};
/* Thêm task và listTasks */
document.getElementById("addItem").addEventListener("click", () => {
    let task = document.getElementById("newTask").value;
    let checkRq = checkInputRequired(task);
    if (checkRq === false) {
        return;
    }
    let id = 0;
    if (listTask.length === 0) {
        id = 1
    } else {
        id = listTask.arr.length + 1;
    }
    let isActive = 1;
    let newTask = new Task(id, task, isActive);
    listTask.addTask(newTask);
    saveToLocalStorage(listTask);
    renderTask(listTask)
});
/** Delete a task */
const deleteTask = (id) => {
    listTask.removeTask(id);
    saveToLocalStorage(listTask);
    renderTask(listTask);
}
window.deleteTask = deleteTask;
/**completed Task */
const completeTask = (id) => {

    listTask.completedTask(id);
    saveToLocalStorage(listTask);
    renderTask(listTask);
}
window.completeTask = completeTask;